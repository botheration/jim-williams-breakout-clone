
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

//Lays brics, on the server
public class BrickLayerController : NetworkBehaviour
{
    private int numBricksRemaining = 0;

    public GameObject prefabBrickRed;
    public GameObject prefabBrickGreen;
    public GameObject prefabBrickBlue;
    public GameObject prefabBrickYellow;
    public GameObject prefabBrickMagenta;



    private float leftLimit = 0; // -5.5f;
    internal float LeftLimit { get => leftLimit; set => leftLimit = value; }
    private float rightLimit = 0;//5.5f;
    internal float RightLimit { get => rightLimit; set => rightLimit = value; }
    private float brickDefaultSizeX = 0;//1.0f;
    internal float BrickDefaultSizeX { get => brickDefaultSizeX; set => brickDefaultSizeX = value; }
    private float brickDefaultSizeY = 0.5f;
    internal float BrickDefaultSizeY { get => brickDefaultSizeY; set => brickDefaultSizeY = value; }

    void Start()
    {
       ;
    }

    public override void OnStartServer()
    {
        Invoke("MakeBrickWall", 0.1f);
    }


   
    //make a wall of bricks, from multiple rows
    void MakeBrickWall()
    {
        float startx = LeftLimit;
        float endx = RightLimit;
        float y = 4;

        float gap = BrickDefaultSizeX * 0.1f;

        
        MakeBrickRow(startx, endx, y, prefabBrickRed);
        y += BrickDefaultSizeY + gap;      
        MakeBrickRow(startx, endx, y, prefabBrickGreen);
        y += BrickDefaultSizeY + gap;
        MakeBrickRow(startx, endx, y, prefabBrickYellow);
        y += BrickDefaultSizeY + gap;
        MakeBrickRow(startx, endx, y, prefabBrickMagenta);
        y += BrickDefaultSizeY + gap;
        MakeBrickRow(startx, endx, y, prefabBrickBlue);
       

        GameObject[] bricksCreated = GameObject.FindGameObjectsWithTag("brick");
        numBricksRemaining = bricksCreated.Length;

    }


    //Make a row of random width bricks, that fills between startX,  endX
    private void MakeBrickRow(float startX, float endX, float y, GameObject prefabBrick)
    {

        Vector3 pos = new Vector3(startX, y, -0.5f);

        float prevPosX = startX;
        float prevBrickXSize = 0;
        bool finished = false;
        float gap = BrickDefaultSizeX * 0.1f;
        while (!finished)
        {
            float remainingX = endX - (prevPosX + (0.5f * prevBrickXSize));

            float brickXSize;
            if (remainingX < BrickDefaultSizeX * 1.5)
            {
                //Brick size to fill remaning gap
                brickXSize = remainingX - gap;
                finished = true;
            }
            else
            {
                //random brick size
                brickXSize = Random.Range(BrickDefaultSizeX * 0.5f, BrickDefaultSizeX);
            }
            //make pos of new brick from size of old brick and size of new brick
            float brickPosX = prevPosX + (0.5f * prevBrickXSize) + (brickXSize * 0.5f) + gap;

            pos.x = brickPosX;

            GameObject brick = Instantiate(prefabBrick, pos, Quaternion.identity) ;

            brick.transform.localScale = new Vector3(brickXSize, BrickDefaultSizeY, 1);

            prevBrickXSize = brickXSize;

            prevPosX = pos.x;
           

            NetworkServer.Spawn(brick);
        }
    }



}