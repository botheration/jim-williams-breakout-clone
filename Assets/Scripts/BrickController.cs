using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

//Controller for the brick!
public class BrickController :  MonoBehaviour
{
    private GameController gameController;

    // Start is called before the first frame update
    void Start()
    {
        gameController = FindObjectOfType<GameController>();      
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    void OnCollisionExit2D(Collision2D other)
    {
        // Debug.Log("Brick Collision!");
        if (other.gameObject.tag == "ball")
        {           
            // Debug.Log("Brick Collision!");                       
            Destroy(gameObject, 0.1f);
            gameController.BrickSmashed();            
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {

    }

}
