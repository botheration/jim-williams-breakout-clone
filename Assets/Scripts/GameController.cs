using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

//Game controller class for Jim Williams Breakout clone
public class GameController : NetworkBehaviour 
{
    private BatController batController;
    private BallController ballController;
    private UIManager uiManager;
    private BrickLayerController brickLayerController;

    private AudioSource audioSource;
    public AudioClip clipBrickExplode;
    public AudioClip clipBallHit;
    public AudioClip clipBallLost;
    public AudioClip clipWin;

    public GameObject prefabBrick;

    public GameObject leftWall;
    public GameObject rightWall;

    [SyncVar]
    private int score;

    private int brickScore = 100;//score per brick


    //Start positions for the 2 bats
    private Vector3 batStartPosition;
    private Vector3 ballStartPosition;

 
    public float BrickDefaultSizeX
    {
        get { return prefabBrick.transform.localScale.x; }
    }

    public float BrickDefaultSizeY
    {
        get { return prefabBrick.transform.localScale.y; }
    }

    //Get the Left limit of the playing area
    float leftLimit = float.MaxValue;
    public float LeftLimit
    {       
        get
        {
            if (leftLimit == float.MaxValue)
            {
                Renderer rend = leftWall.GetComponent<Renderer>();
                Bounds bounds = rend.bounds;
                leftLimit = bounds.max.x;
            }
            return leftLimit;
        }

    }

    //Get the Right limit of the playing area
    float rightLimit = float.MaxValue;
    public float RightLimit
    {     
        get
        {
            if (rightLimit == float.MaxValue)
            {
                Renderer rend = rightWall.GetComponent<Renderer>();
                Bounds bounds = rend.bounds;
                rightLimit = bounds.min.x;
            }
            return rightLimit;
        }

    }


    // Start is called before the first frame update
    void Start()
    {
        //Get the objects we need to know about
        uiManager = FindObjectOfType<UIManager>();

        batController = FindObjectOfType<BatController>();
        ballController = FindObjectOfType<BallController>();
        audioSource = GetComponent<AudioSource>();

        brickLayerController = FindObjectOfType<BrickLayerController>();
        brickLayerController.LeftLimit = LeftLimit;
        brickLayerController.RightLimit = RightLimit;
        brickLayerController.BrickDefaultSizeX = BrickDefaultSizeX;
        brickLayerController.BrickDefaultSizeY = BrickDefaultSizeY;

        StartLevel();
    }


    public void PlayBallLostAudio()
    {
        audioSource.PlayOneShot(clipBallLost, 1.0f);
    }

    void StartLevel()
    {
        score = 0;
        uiManager.SetScore(score);      
    }


    public void SetBallVelocity(Vector2 velocity)
    {
        ballController.SetVelocity(velocity);
    }

    //Called when the ball hits something - other than a brick
    public void BallHit()
    {
        audioSource.PlayOneShot(clipBallHit, 1.0f);
    }

    //Called when a brick is smashed
    public void BrickSmashed()
    {
        audioSource.PlayOneShot(clipBrickExplode, 1.0f);//play clip here rather than from brick as we destroy it!

        if (this.isServer)
        {
            score += brickScore;
        }
        //  Debug.Log("BrickSmashed. Score = "  + score);
        uiManager.SetScore(score);


    }
    // Update is called once per frame
    void Update()
    {
        uiManager.SetScore(score);//continuously update, so shows server score 
    }
}

