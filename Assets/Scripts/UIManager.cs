using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

//Manages the user interface!
public class UIManager : MonoBehaviour
{
    public TMP_Text scoreText;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    public void SetScore(int score)
    {
        scoreText.text = score.ToString("D6");
    }
}
