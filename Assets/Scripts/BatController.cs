using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class BatController : NetworkBehaviour
{
    private Rigidbody2D batRigidbody;
    private float batSpeed = 5.0f;
    private float epsilon = 0.05f;
    private GameController gameController;
    public GameObject prefabBall;
    private float ballSpeedLaunchSpeed = 2.0f;
    Vector2 movement;

    private PlayerState playerState;
    internal PlayerState PlayerState { get => playerState; set => playerState = value; }

    public float BatSizeX
    {
        get { return transform.localScale.x; }
    }

    public float HalfBatSizeX
    {
        get { return BatSizeX * 0.5f; }
    }

    // Start is called before the first frame update
    void Start()
    {
        gameController = FindObjectOfType<GameController>();


        batRigidbody = GetComponent<Rigidbody2D>();

        if (isLocalPlayer)
        {
            PlayerState = PlayerState.WaitingToStart;
           // Debug.Log("PlayerState =  PlayerState.WaitingToStart");
        }
    }

    // Update is called once per frame
    void Update()
    {
        ReadInput();      
    }


    //Read input for keyboard and update movement vector, and launch ball
    void ReadInput()
    {
        float hInput = Input.GetAxisRaw("Horizontal");
        float vInput = Input.GetAxisRaw("Vertical");

        movement = new Vector2(0, 0);

        if (hInput < 0)
        {
            //Limit movement so don't run bat into walls
            if ((batRigidbody.position.x - HalfBatSizeX - epsilon) > gameController.LeftLimit)
            {
                movement = new Vector2(-batSpeed, 0);
            }
        }
        else if (hInput > 0)
        {
            if ((batRigidbody.position.x + HalfBatSizeX + epsilon) < gameController.RightLimit)
            {
                movement = new Vector2(batSpeed, 0);
            }
        }

        if ((vInput > 0) || (Input.GetKey(KeyCode.Space)))
        {
            if ((PlayerState == PlayerState.WaitingToStart) && this.isLocalPlayer)
            {
                gameObject.transform.rotation = Quaternion.Euler(new Vector3(0, 180, 0)); //rotate the bat so the fake ball is invisible!

                ShootBall();
                PlayerState = PlayerState.Playing;
               // Debug.Log("PlayerState =  PlayerState.Playing");

            }

        }

    }

    public void Restart()
    {
        gameController.PlayBallLostAudio();
        if (isLocalPlayer)
        {
            PlayerState = PlayerState.WaitingToStart;
          //  Debug.Log("PlayerState =  PlayerState.WaitingToStart");
            gameObject.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0)); //rotate the bat so the fake ball is visible!
        }

    }


    [Command]
    void ShootBall()
    {
        //Start the ball just off the bat
        Vector3 startpos = new Vector3(transform.position.x, transform.position.y + 0.2f, transform.position.z);
        GameObject ball = Instantiate(prefabBall, startpos, Quaternion.identity);

        BallController ballController = ball.GetComponent<BallController>();
        ballController.Bat = this.gameObject;

        Rigidbody2D ballRigidbody = ball.GetComponent<Rigidbody2D>();
        ballRigidbody.velocity = LaunchVector() * ballSpeedLaunchSpeed;       

        NetworkServer.Spawn(ball);    
    }



    //Generate a random vector for an angle between 45 and 135 degrees, a 90 triangle above bat
    private Vector2 LaunchVector()
    {
        Vector2 result;
        float angleDeg = Random.Range(45, 135);//90 deg triangle
        float angleRad = angleDeg * Mathf.Deg2Rad;
        float x = Mathf.Cos(angleRad);
        float y = Mathf.Sin(angleRad);
        result = new Vector2(x, y);
        return result;
    }

    void FixedUpdate()
    {
        if (this.isLocalPlayer)
        {
            batRigidbody.velocity = movement;            
        }
    }
}

