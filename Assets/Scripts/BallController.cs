using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class BallController : NetworkBehaviour
{
    private Rigidbody2D ballRigidbody;


    //Min speed for the ball
    private float minSpeedX = 0.75f;
    private float minSpeedY = 0.75f;
    

    private GameController gameController;

    private GameObject bat;
    public GameObject Bat
    {
        get { return bat; }
        set {  bat = value; }
    }

  
    // Start is called before the first frame update
    void Start()
    {
        gameController = FindObjectOfType<GameController>();

        ballRigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
            
    }


    void FixedUpdate()
    {
      
    }


   
    public void SetVelocity(Vector2 velocity)
    {
        ballRigidbody.velocity =  velocity;
    }



    void EnsureMinXMinYSpeeds()
    {
        if (this.isServer)
        { 
            BatController batController = GetBatController();
            if (batController == null) return;
         
           // Debug.Log("EnsureMinXMinYSpeeds ... ");
            //Speed the ball up if it gets too slow in X or Y,  - give the physics a helping hand
            //Do X and Y separately, as might want them to be different
            if ((Mathf.Abs(ballRigidbody.velocity.x) < minSpeedX) || (Mathf.Abs(ballRigidbody.velocity.y) < minSpeedY))
            {
                Vector2 newVel = new Vector2(ballRigidbody.velocity.x, ballRigidbody.velocity.y);
                if (Mathf.Abs(newVel.x) < minSpeedX)
                {
                    if (newVel.x >= 0)
                    {
                        newVel.x = minSpeedX;
                    }
                    else
                    {
                        newVel.x = -minSpeedX;
                    }
                   //  Debug.Log("newVel.x to " + newVel.x);
                }
                if (Mathf.Abs(newVel.y) < minSpeedY)
                {
                    if (newVel.y >= 0)
                    {
                        newVel.y = minSpeedY;
                    }
                    else
                    {
                        newVel.y = -minSpeedY;
                    }
                     //   Debug.Log("newVel.y to " + newVel.y);
                }
                ballRigidbody.velocity = newVel;
            }            
        }
    }
    

    
    BatController GetBatController()
    {
        BatController batController = null;
        if(this.Bat != null)
        {
            batController = this.Bat.GetComponent<BatController>();
           // Debug.Log("Stored Bat FOUND");
        }
        else
        {
           // Debug.Log("Stored Bat Null!");
        }
        return batController;
    }

 

    void OnCollisionEnter2D(Collision2D other)
    {
        if(this.isServer)
        {
          //  Debug.Log("Ball Collision!");

            //make sure collision doesn't slow too much in x or y, or ball may become unplayable, parallel or x or y
            EnsureMinXMinYSpeeds();

            if ((other.gameObject.tag == "bat") || (other.gameObject.tag == "wall"))//brick handles sound for itself
            {
                gameController.BallHit();
            }

            if (other.gameObject.tag == "deathwall")
            {
                Debug.Log("DeathWall");


                BatController batController = GetBatController();

                if (batController != null)
                {                   
                    RpcBatControllerRestart(batController);
                   
                    Destroy(gameObject, 0.1f);
                }

            }
        }
        
    }

    [ClientRpc]
    void RpcBatControllerRestart(BatController batController)
    {      
        batController.Restart();     
    }



}
